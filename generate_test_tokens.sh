#!/bin/sh
# generate_test_tokens.sh
# 2023-03-17 | CR
#
# Run with:
# bash generate_test_tokens.sh
#
cat .env
set -o allexport
source .env
set +o allexport
echo "id,url,lak,lac,lpc,data" > ${CSV_FILENAME}
for i in {1..10}; do sh generate_test_token_one.sh >> ${CSV_FILENAME} ; done
echo "---------------------"
cat ${CSV_FILENAME}

