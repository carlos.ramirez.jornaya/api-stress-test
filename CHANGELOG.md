

# CHANGELOG

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/) and [Keep a Changelog](http://keepachangelog.com/).


## Unreleased
---

### New

### Changes

### Fixes

### Breaks


## 0.1.3 - (2023-04-21)
---

### New
Add: output csv file generation.
Add: full input csv file generation with fresh tokens in "generate_test_tokens.sh".
Add: CHANGELOG introduced.


## 0.1.2 - (2022-12-22)
---

### Changes
Add: date/time included in the output report
Add: Date/time added in the general list

### Fixes
Fix: Thread ID fixed in error/debug report


## 0.1.1 - (2022-11-08)
---

### New
Add: .env.example file to help configure the tool

### Changes
Add: extra instructions added to README to use MAX_OPEN_THREADS

### Fixes
Fix: invalid_params error added
Fix: extra unnedded debug removed
Fix: urllib.parse.quote and parameters encoding removed


## 0.1.0 - (2022-11-04)
---

### New
Add: Initial commit
Add: README instructions.

### Fixes
Fix: error handling for each Thread call.
Fix: debug info to report API call & response only
Fix: api call params encoded
Fix: csv could not determine delimiter
