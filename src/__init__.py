#!/usr/bin/python3
from api_stress_test_lib.api_stress_test_lib import main

# __init__.py
# API stress test main module
# 2022-10-28 - CR


def detect_api_errors(api_response):
    error_found = False
    error_found = error_found or 'error' in api_response
    error_found = error_found or 'error' in api_response.get('audit', {})
    error_found = error_found or (
        'authentic' in api_response.get('audit', {}) and
        api_response['audit']['authentic'] == 0
    )
    error_found = error_found or (
        'error' in api_response.get('audit', {}).get('market', {}).get('ConsumerData', {}).get('IDScores', {})
    )
    error_found = error_found or (
        'error' in api_response.get('audit', {}).get('market', {}).get('ConsumerData', {}).get('ConsumerAttributes', {})
    )
    # 'invalid_params': ['infutor_poc']
    error_found = error_found or (
        'infutor_poc' in api_response.get('audit', {}).get('invalid_params', {})
    )
    return error_found


if __name__ == "__main__":
    main(detect_api_errors)
