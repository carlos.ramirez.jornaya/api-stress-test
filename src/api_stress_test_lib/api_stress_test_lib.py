# api_stress_test_lib.py
# 2022-10-28 - CR

import _thread
import threading
import time
import csv
import os
from dotenv import load_dotenv
import requests
# import urllib
from datetime import datetime

thread_counter = 0
output = None
output_lock = None

CSV_FILENAME = 'stress_test.csv'
OUTPUT_FILENAME = ''
MAX_OPEN_THREADS = 200
SLEEP_SECONDS_FOR_NEXT_THREAD_OPENING = 3


def csv_init(file_handler):
    file_handler.seek(0)
    # sample = file_handler.read(1024)
    sample = file_handler.readline()
    dialect = CSV_DIALECT if CSV_DIALECT != '' else csv.Sniffer().sniff(sample)
    has_header = csv.Sniffer().has_header(sample)
    headers = []
    file_handler.seek(0)
    reader = csv.reader(file_handler, dialect)
    if has_header:
        headers = next(reader)
    return headers, reader


def csv_read_line(raw_row, headers):
    row = dict()
    for index, value in enumerate(raw_row):
        row[index if not headers else headers[index]] = value
    return row


def output_file_init(file_name):
    global output
    global output_lock
    if DEBUG_FLAG:
        print(f"Initializing the output filename '{file_name}'...")
    if file_name != '':
        output = open(file_name, 'w')
        output_lock = threading.Lock()
        with output_lock:  # this will block until the lock is available
            print(
                'test_second' +
                ',' +
                'call_counter' +
                ',' +
                'datetime' +
                ',' +
                'error_found' +
                ',' +
                'api_call' +
                ',' +
                'api_response',
                file=output
            )
            if DEBUG_FLAG:
                print(f"Output filename '{file_name}' has been initialized...")


class LeadidIter():
    def __init__(self, file_handler):
        self.file_handler = file_handler
        self.headers, self.csv_reader = csv_init(self.file_handler)
        self.counter = 0

    def __iter__(self):
        self.counter = 0
        return self

    def __next__(self):
        raw_row = next(self.csv_reader, False)
        if raw_row is False:
            self.headers, self.csv_reader = csv_init(self.file_handler)
            raw_row = next(self.csv_reader)
        next_row = csv_read_line(raw_row, self.headers)
        return next_row


def encode_parameter(param_value):
    param_value = param_value.replace(' ', '%20')
    return param_value


def one_api_call(
    api_call_params,
    thread_id,
    test_second,
    call_counter,
    detect_api_errors_function
):
    global thread_counter
    global output
    global output_lock
    error_found = False
    error_message = ''
    api_response = None
    # if DEBUG_FLAG:
    #     print('|||--> api_call_params:', api_call_params)
    url = api_call_params['url']
    params = dict(api_call_params)
    del (params['url'])
    # params = {key: urllib.parse.quote(value) for (key, value) in params.items()}
    # params = {key: encode_parameter(value) for (key, value) in params.items()}
    try:
        resp = requests.get(url=url, params=params)
    except Exception as err:
        error_found = True
        error_message = ["Error: unable to call the API", err]
    if not error_found:
        try:
            api_response = resp.json()
        except Exception as err:
            error_found = True
            error_message = ["Error: unable to convert the API response to JSON", err]
    if not error_found:
        error_found = detect_api_errors_function(api_response)
        if error_found:
            error_message = ["Check the API response..."]
    if DEBUG_FLAG or error_found:
        print(
            '\n*** Thread ID:', thread_id,
            '\nTest second:', test_second,
            '\nCall counter:', call_counter,
            '\nDate/time:', datetime.now(),
            '\nError:', error_found,
            '\nAPI call:\n', url, params,
            '\nAPI response:\n', api_response,
            '\nError message:\n', error_message,
        )
    if output:
        with output_lock:  # this will block until the lock is available
            print(
                str(test_second) +
                ',' +
                str(call_counter) +
                ',' +
                f'"\'{datetime.now()}"' +
                ',' + 
                str(error_found) +
                ',' +
                f'"{url}?{"&".join(f"{k}={v}" for k,v in params.items())}"' +
                ',' +
                f'"{api_response}"',
                file=output
            )
            if DEBUG_FLAG:
                print(f"Output file line written...")

    thread_counter -= 1


def stress_api(file_handler, detect_api_errors_function):
    global thread_counter
    lead_id_list = LeadidIter(file_handler)

    for test_second in range(1, TOTAL_TEST_TIME_IN_SECONDS+1):
        start_timer = time.time()
        for call_counter in range(1, TIMES_PER_SECOND+1):
            one_lead_id = next(lead_id_list)
            while thread_counter >= MAX_OPEN_THREADS:
                print(
                    thread_counter,
                    'opened Threads. Waiting', 
                    SLEEP_SECONDS_FOR_NEXT_THREAD_OPENING,
                    'seconds to open next Thread...',
                )
                time.sleep(SLEEP_SECONDS_FOR_NEXT_THREAD_OPENING)
            try:
                thread_counter += 1
                _thread.start_new_thread(
                    one_api_call,
                    (
                        one_lead_id,
                        thread_counter,
                        test_second,
                        call_counter,
                        detect_api_errors_function,
                    )
                )
            except Exception as err:
                thread_counter -= 1
                print("Error: unable to start Thread", err)
            end_timer = time.time()
            seconds_elapsed = end_timer - start_timer
            print(
                'Thread ID:', thread_counter,
                '| Test second:', test_second,
                '| Call counter:', call_counter,
                '| Elapsed:', repr(seconds_elapsed), 'seconds',
                '| Date/time:', datetime.now()
            )
            if seconds_elapsed >= 1.00:
                break
        waiting_until_second_ends = 1.00 - seconds_elapsed
        if waiting_until_second_ends > 0:
            print('Waiting:', waiting_until_second_ends, 'seconds for next Second round...')
            time.sleep(waiting_until_second_ends)

    while thread_counter:
        print('Threads left:', thread_counter)
        time.sleep(SLEEP_SECONDS_WAIT_TO_FINISH)


def read_env_vars():
    global TIMES_PER_SECOND, TOTAL_TEST_TIME_IN_SECONDS, SLEEP_SECONDS_WAIT_TO_FINISH
    global DEBUG_FLAG, CSV_FILENAME, CSV_DIALECT
    global SLEEP_SECONDS_FOR_NEXT_THREAD_OPENING, MAX_OPEN_THREADS
    global OUTPUT_FILENAME

    load_dotenv()

    TIMES_PER_SECOND = int(os.environ.get('TIMES_PER_SECOND', '20'))  # 20
    TOTAL_TEST_TIME_IN_SECONDS = int(os.environ.get('TOTAL_TEST_TIME_IN_SECONDS', '2'))  # 5*60
    SLEEP_SECONDS_WAIT_TO_FINISH = int(os.environ.get('SLEEP_SECONDS_WAIT_TO_FINISH', '2'))
    DEBUG_FLAG = int(os.environ.get('DEBUG_FLAG', '0'))
    CSV_FILENAME = os.environ.get('CSV_FILENAME', 'audit_stress_test_LOCAL.csv')
    OUTPUT_FILENAME = os.environ.get('OUTPUT_FILENAME', '')
    CSV_DIALECT = os.environ.get('CSV_DIALECT', '')  # excel-tab
    MAX_OPEN_THREADS = int(os.environ.get('MAX_OPEN_THREADS', '200'))
    SLEEP_SECONDS_FOR_NEXT_THREAD_OPENING = \
        int(os.environ.get('SLEEP_SECONDS_FOR_NEXT_THREAD_OPENING', '3'))

    print('TIMES_PER_SECOND =', TIMES_PER_SECOND)
    print('TOTAL_TEST_TIME_IN_SECONDS =', TOTAL_TEST_TIME_IN_SECONDS)
    print('SLEEP_SECONDS_WAIT_TO_FINISH =', SLEEP_SECONDS_WAIT_TO_FINISH)
    print('DEBUG_FLAG =', DEBUG_FLAG)
    print('CSV_FILENAME =', CSV_FILENAME)
    print('OUTPUT_FILENAME =', OUTPUT_FILENAME)
    print('CSV_DIALECT =', CSV_DIALECT)
    print('MAX_OPEN_THREADS =', MAX_OPEN_THREADS)
    print('SLEEP_SECONDS_FOR_NEXT_THREAD_OPENING =',
          SLEEP_SECONDS_FOR_NEXT_THREAD_OPENING)
    print('')


def main(detect_api_errors_function):
    read_env_vars()
    output_file_init(OUTPUT_FILENAME)
    with open(CSV_FILENAME, newline='', encoding='utf-8') as file_handler:
        stress_api(file_handler, detect_api_errors_function)
