# api-stress-test

This Python program can be used as a stress test tool for any API.
The ttol reads a CSV file with the API Url and the parameters to be send.
It can be one or more rows of Url + parameters, and restart from the begining when it reaches the end of the list if more API calls are needed.
It reports only responses with errors, unless the debug mode is on.

## Cloning the repo
 
Open a Terminal window and clone the repo on your local environment.
Please change the "./git_repos" folder name with your repos' parent folder name.

```bash
cd ./git_repos
git clone https://gitlab.com/carlos.ramirez.jornaya/api-stress-test.git
cd api-stress-test
```

## Create the configuration file

Create an ".env" file to configure the app.
The .env file must be in the same directory of the ".env.example" file.
You can use the ".env.example" file as a template.

```bash
cp .env.example .env
```

To configure the `audit_stress_test_QA.csv` input file, change the `CSV_FILENAME` parameter:

```.env
CSV_FILENAME=audit_stress_test_QA.csv
```

If you want to generate a CSV file with the tool run results, configure the the `OUTPUT_FILENAME` parameter. Example, to generate the `audit_stress_test_result_LOCAL.csv` output file:

```.env
OUTPUT_FILENAME=audit_stress_test_result_LOCAL.csv
```

If the `OUTPUT_FILENAME` parameter is empty, no output file will be generated

When it's required a test of 20 API calls each second for a minute, you can configure it like this:

```.env
TIMES_PER_SECOND=20
TOTAL_TEST_TIME_IN_SECONDS=60
```

If you get a lot of any of this errors:

```
Failed to establish a new connection: [Errno 8] nodename nor servname provided, or not known
```

or

```
Failed to establish a new connection: [Errno 24] Too many open files
```

or

```
Error: unable to convert the API response to JSON', JSONDecodeError('Expecting value: line 1 column 1 (char 0)
```

Decreasing the maximun files open parameter can help:

```.env
MAX_OPEN_THREADS=150
```

There's a debug mode configured by th DEBUG_FLAG parameter.
When debug mode is on, the program reports all the API calls and its responses.
When debug mode is off, the program reports only those responses with errors.
You can turn on the debug mode by changing the DEBUG_FLAG in the ".env" file:

```.env
DEBUG_FLAG=1
```

To use the input file generation script `generate_test_tokens.sh`, configure these parameters:

GTT_CREATE_URL="https://<CREATE_API_ENDPOINT>?_=12345&pid=feedbeef-beef-beef-beef-feedbeefcafe&msn=1"
GTT_AUDIT_URL=https://<AUDIT_API_ENDPOINT>
GTT_LAC=XXXXXX
GTT_LCK=XXXXXX
GTT_LAK=XXXXXX
GTT_LPC=XXXXXX
GTT_DATA="xxxx;yyyy|xxxx;yyyy"
GTT_REF_PAR="ref=http%3A%2F%2Fjsfiddle.net%2Fmalip%2Fxxxx"
GTT_URL_PAR="url=http%3A%2F%2Ffiddle.jshell.net%2Fmalip%2Fxxxx%2Fshow%2F"
GTT_CLIENT_TIME_PAR="client_time=1444778662926"
GTT_INFRAME="inFrame=true"


## Create the input file aumatically

Run the input file generation script with the command:

```bash
bash generate_test_tokens.sh
```

It will create a file configured in the `` environment variable.

By default it generates 10 tokens.
If you want more tokens, change this line:

```bash
for i in {1..10}; ...
```

## Create the input file manually

Create your .csv input file with the API URL and parameters.
For example you can create a file named "audit_stress_test_QA.csv".
You can use the "audit_stress_test.example.csv" file as a template.

```bash
cp audit_stress_test.example.csv audit_stress_test_QA.csv
```

Then open the "audit_stress_test_QA.csv" file with MS Excel or a text editor, and populate it with your required data.

The first row have the parameter names.
The 'url' is mandatory, because it's the API endpoint URL.
The rest of the columns are the parameters will be passed to the API.

The .csv input file must be in the same directory of the "audit_stress_test.example.csv" file.

# Run the tool

Finally run the stress test program in a Terminal window:

```bash
sh run.sh
```


## Updating the repo

Open a Terminal window, go to the repo local folder and perform a `git pull`.
Please change the "./git_repos" folder name with your repos' parent folder name.

```bash
cd ./git_repos/api-stress-test
git pull
```