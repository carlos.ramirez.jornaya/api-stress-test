#!/bin/sh
# generate_test_token_one.sh
# 2023-03-17 | CR
#
# id,url,lak,lac,lpc,data
curl --location ${GTT_CREATE_URL} \
    --header 'Content-Type: application/x-www-form-urlencoded' \
    --header 'Cookie: rgisanonymous=true; rguserid=c09e8c4d-5b4f-42bf-b9d6-8d550bacf597; rguuid=true' \
    --data-urlencode "lac=${GTT_LAC}" \
    --data-urlencode "lck=${GTT_LCK}" \
    --data-urlencode ${GTT_REF_PAR} \
    --data-urlencode ${GTT_URL_PAR} \
    --data-urlencode ${GTT_CLIENT_TIME_PAR} \
    --data-urlencode ${GTT_INFRAME}
echo ",${GTT_AUDIT_URL},${GTT_LAK},${GTT_LAC},${GTT_LPC},${GTT_DATA}"
